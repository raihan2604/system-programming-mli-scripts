# MLI System Programming Group Project Scripts

This repository consists of scripts used in MLI System Programming Group Project.

## CLI To Control Sound Card Driver

audio_controller_rev.sh is a script of CLI Application to control Sound Card Driver. To use this script, run this script
as root (sudo).
```
sudo /path/to/audio_controller_rev.sh
```

With this script, you can :
- Enable/disable the sound card
- Manage audio volume up/down when sound card is enabled.

NOTE : To use this script, make sure kernel module used for your audio device is snd_hda_intel (see with 'lspci -v' command),
and make sure alsa-base is installed.

## Device Driver Simulation Scripts

We use the same script as the script in WorkSheet 11 (Week 13). We simulate the use of character device and can receive and
return what user types in the tester file. To run the script, first execute make command.
```
make
```

Install the module in the kernel with modprobe command.
```
modprobe mr_drv.ko
```

Run the executable tester file and type anything. If the module functions right, you can see what you typed returned after 
you hit enter.

## Bootscript

To run our CLI Sound Card Application at booting, we add a command to execute our CLI Script in the .bashrc file of our user.
Place the .bashrc to our user $HOME, and thats it, our CLI will run everytime we login to our account.
