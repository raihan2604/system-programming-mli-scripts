#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/cdev.h>

#define  DEVICE_NAME "mlidrv"    // /dev/<value>
#define  CLASS_NAME  "mlic"          // The device class

MODULE_LICENSE("GPL");
MODULE_AUTHOR("MLI Group");
MODULE_DESCRIPTION("simple Linux char driver");
MODULE_VERSION("0.1");
static dev_t mr_dev_t = 0;
static struct class* mr_class;
static struct cdev* mr_cdev;
static char   message[256] = {0};
static short  size_of_message;
static int    numberOpens = 0;
static int result=-1;

// The prototype functions for the character driver
static int     dev_open(struct inode *, struct file *);
static int     dev_release(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);

static struct file_operations fops =
{
   .open = dev_open,
   .read = dev_read,
   .write = dev_write,
   .release = dev_release,
};


static int dev_open(struct inode *inodep, struct file *filep){
   numberOpens++;
   printk(KERN_INFO "MRDriver: Device has been opened %d time(s)\n", numberOpens);
   return 0;
}

static int dev_release(struct inode *inodep, struct file *filep){
   printk(KERN_INFO "MRDriver: Device successfully closed\n");
   return 0;
}


static int __init rdchardrv_init(void){
   result = alloc_chrdev_region(&mr_dev_t, 0, 1, DEVICE_NAME);
    if(result < 0) {
	printk(KERN_ERR "failed to alloc chrdev region\n");
	goto fail_alloc_chrdev_region;
    }
    mr_cdev = cdev_alloc();
    if(!mr_cdev) {
	result = -ENOMEM;
	printk(KERN_ERR "failed to alloc cdev\n");
	goto fail_alloc_cdev;
    }
    cdev_init(mr_cdev, &fops);
    result = cdev_add(mr_cdev, mr_dev_t, 1);
    if(result < 0) {
	printk(KERN_ERR "failed to add cdev\n");
	goto fail_add_cdev;
    }
    mr_class = class_create(THIS_MODULE, CLASS_NAME);
    if(!mr_class) {
	result = -EEXIST;
	printk(KERN_ERR "failed to create class\n");
	goto fail_create_class;
    }
    if(!device_create(mr_class, NULL, mr_dev_t, NULL, DEVICE_NAME, MINOR(mr_dev_t))) {
	result = -EINVAL;
	printk(KERN_ERR "failed to create device\n");
	goto fail_create_device;
    }
    printk(KERN_INFO "MRDriver : init successful\n");
    return 0;
fail_create_device:
    class_destroy(mr_class);
fail_create_class:
    cdev_del(mr_cdev);
fail_add_cdev:
fail_alloc_cdev:
    unregister_chrdev_region(mr_dev_t, 1);
fail_alloc_chrdev_region:
    return result;
}


static void __exit rdchardrv_exit(void){
    printk(KERN_INFO "MRDriver : driver exit\n");
    device_destroy(mr_class, mr_dev_t);
    class_destroy(mr_class);
    cdev_del(mr_cdev);
    unregister_chrdev_region(mr_dev_t, 1);
}

static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset){
    // precaution
    size_t bytesToCopy = len >= size_of_message ? size_of_message: len;
    size_t bytesNotCopied = 0;
    if(!bytesToCopy) return 0;

bytesNotCopied = raw_copy_to_user(buffer, message,
bytesToCopy);
    if(bytesNotCopied){
         return -EFAULT;
    }
    size_of_message = 0;
    return bytesToCopy;
}


static ssize_t dev_write(struct file *filep,
const char *buffer, size_t len, loff_t *offset){

    const size_t maxLen = 256 - 1;
    size_t bytesToCopy = len >= maxLen ? maxLen: len;
    size_t bytesNotCopied = 0;

    bytesNotCopied = raw_copy_from_user(message, buffer,bytesToCopy);
    size_of_message = bytesToCopy - bytesNotCopied;

    if(bytesNotCopied)
            return -EFAULT;
    return bytesToCopy;
}

module_init(rdchardrv_init);
module_exit(rdchardrv_exit);
