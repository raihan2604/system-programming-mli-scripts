#!/bin/bash

#Enabling Intel Audio
#modprobe -r snd_hda_intel
#modprobe snd_hda_intel enable=Y 

declare -i CONNECTED=1

#Main Menu
while [ $CONNECTED -eq 1 ]
do

 

  declare -i MENU=1
 

  while [[ $MENU -eq 1 ]]
    do

      for i in {0..50..1}
	    do
	      printf "="
	    done
	  printf "\n"

	  for i in {0..50..1}
	    do
	    	if [[ $i -eq 19 ]]
	    	then	
	        		printf "MAIN MENU"
	  	else
	  		printf " "
	      fi
	    done
	  printf "\n"

	  for i in {0..50..1}
	    do
	        printf "="
	    done
	  printf "\n"
     #Getting Current Audio Status
	 STAT=$(grep -H '' /sys/module/snd_hda_intel/parameters/enable | awk -F ':' '{ print $2 }' | awk -F ',' '{ print $1 }')

     if [[ "$STAT" == "N" ]]
	  then
		  printf "Audio is Disable\n"
		  printf "1. Turn On Audio\n"
		  printf "2. Exit\n"
		  read -p 'choose 1-2: ' userinput

		  if [[ $userinput -eq 1 ]]
		  then
		  	  #Enabling Intel Audio
			  modprobe -r snd_hda_intel
			  modprobe snd_hda_intel enable=Y 

               

              declare -i MANAGE=1
              while [ $MANAGE -eq 1 ]
              	do

              		   for i in {0..50..1}
		                do
		                  printf "="
		                done
		              printf "\n"

		              for i in {0..50..1}
		                do
		                  if [[ $i -eq 19 ]]
		                  then  
		                        printf "Manage Audio"
		                else
		                  printf " "
		                  fi
		                done
		              printf "\n"

		              for i in {0..50..1}
		                do
		                    printf "="
		                done

		              printf "\n"    
					  printf "Audio is enable\n"
					  printf "1. Volume Up\n"
					  printf "2. Volume Down\n"
					  printf "3. Turn Off Audio\n"
					  printf "4. Exit\n"
					  read -p 'choose 1-4: ' manage

					  if [[ $manage -eq 1 ]]
					  then
			             for i in {0..50..1}
			                do
			                  printf "="
			                done
			              printf "\n"

			              for i in {0..50..1}
			                do
			                  if [[ $i -eq 19 ]]
			                  then  
			                        printf "Volume Up"
			                else
			                  printf " "
			                  fi
			                done
			              printf "\n"

			              for i in {0..50..1}
			                do
			                    printf "="
			                done
			              printf "\n"					  	


			              declare -i BACK=0
			              while [ $BACK -eq 0 ]
			                do
			                    read -p "Enter The Number: (Write 'back' to go back to Manage Audio Menu) " command
			                 
					    		re='^[0-9]+$'
			                    if [[ "$command" == "back" ]]
			                    then
			                          BACK=1
			           
							    elif [[ $command =~ $re ]]
							    then
			                          amixer set Master "$command"%+
			                    fi
			                done

				        #Volume Down
				        elif [[ $manage -eq 2 ]]
				        then
				              for i in {0..50..1}
				                do
				                  printf "="
				                done
				              printf "\n"

				              for i in {0..50..1}
				                do
				                  if [[ $i -eq 19 ]]
				                  then  
				                        printf "Volume Down"
				                else
				                  printf " "
				                  fi
				                done
				              printf "\n"

				              for i in {0..50..1}
				                do
				                    printf "="
				                done
				              printf "\n"            

				              declare -i BACK=0
				              while [ $BACK -eq 0 ]
				                do
				                    read -p "Enter The Number: (Write 'back' to go back to Manage Audio Menu) " command
				                    
						    		re='^[0-9]+$'
				                    if [[ "$command" == "back" ]]
				                    then
							  			  
				                          BACK=1
				               
						    		elif [[ $command =~ $re ]]
						    		then
				                          amixer set Master "$command"%-
				                    fi
				                done

				        #Turning Off Intel Audio by Disabling It
				        elif [[ $manage -eq 3 ]]
				        then
				              modprobe -r snd_hda_intel
				              modprobe snd_hda_intel enable=N
				              MENU=0
					          MANAGE=0

					    elif [[ $manage -eq 4 ]]
					    then

					    	  printf "Bye Bye\n"
					    	  MENU=0
					    	  MANAGE=0
					    	  CONNECTED=0
				        fi
				  done

			elif [[ $userinput -eq 2 ]]
				then
					printf "Bye Bye"
					MENU=0
					CONNECTED=0
			fi
      
      elif [[ "$STAT" == "Y" ]]
	  then
		  printf "Audio is Already Enable\n"
		  printf "1. Manage Audio\n"
		  printf "2. Turn Off Audio\n"
		  printf "3. Exit\n"
		  read -p 'choose 1-3: ' userinput

		  if [[ $userinput -eq 1 ]]
		  then
		  	   
                          

              declare -i MANAGE=1
              while [ $MANAGE -eq 1 ]
              	do
              		  for i in {0..50..1}
		                do
		                  printf "="
		                done
		              printf "\n"

		              for i in {0..50..1}
		                do
		                  if [[ $i -eq 19 ]]
		                  then  
		                        printf "Manage Audio"
		                else
		                  printf " "
		                  fi
		                done
		              printf "\n"

		              for i in {0..50..1}
		                do
		                    printf "="
		                done
		              printf "\n"
		              
					  printf "Audio is Enable\n"
					  printf "1. Volume Up\n"
					  printf "2. Volume Down\n"
					  printf "3. Turn Off Audio\n"
					  printf "4. Exit\n"
					  read -p 'choose 1-4: ' manage

					  if [[ $manage -eq 1 ]]
					  then
			             for i in {0..50..1}
			                do
			                  printf "="
			                done
			              printf "\n"

			              for i in {0..50..1}
			                do
			                  if [[ $i -eq 19 ]]
			                  then  
			                        printf "Volume Up"
			                else
			                  printf " "
			                  fi
			                done
			              printf "\n"

			              for i in {0..50..1}
			                do
			                    printf "="
			                done
			              printf "\n"					  	


			              declare -i BACK=0
			              while [ $BACK -eq 0 ]
			                do
			                    read -p "Enter The Number: (Write 'back' to go back to Manage Audio Menu) " command
			                 
					    		re='^[0-9]+$'
			                    if [[ "$command" == "back" ]]
			                    then
			                          BACK=1
			           
							    elif [[ $command =~ $re ]]
							    then
			                          amixer set Master "$command"%+
			                    fi
			                done

				        #Volume Down
				        elif [[ $manage -eq 2 ]]
				        then
				              for i in {0..50..1}
				                do
				                  printf "="
				                done
				              printf "\n"

				              for i in {0..50..1}
				                do
				                  if [[ $i -eq 19 ]]
				                  then  
				                        printf "Volume Down"
				                else
				                  printf " "
				                  fi
				                done
				              printf "\n"

				              for i in {0..50..1}
				                do
				                    printf "="
				                done
				              printf "\n"            

				              declare -i BACK=0
				              while [ $BACK -eq 0 ]
				                do
				                    read -p "Enter The Number: (Write 'back' to go back to Manage Audio Menu) " command
				                    
						    		re='^[0-9]+$'
				                    if [[ "$command" == "back" ]]
				                    then
							  			  
				                          BACK=1
				               
						    		elif [[ $command =~ $re ]]
						    		then
				                          amixer set Master "$command"%-
				                    fi
				                done

				        #Turning Off Intel Audio by Disabling It
				        elif [[ $manage -eq 3 ]]
				        then
				              modprobe -r snd_hda_intel
				              modprobe snd_hda_intel enable=N
					      	  MANAGE=0
				              MENU=0

				        elif [[ $manage -eq 4 ]]
				        then
				              printf "Bye Bye\n"
					      	  MANAGE=0
				              MENU=0
				              CONNECTED=0
				        fi
				 done

		   elif [[ $userinput -eq 2 ]]
		   	then

		   		#Turning Off Audio
		   		#Disabling Intel Audio
				modprobe -r snd_hda_intel
				modprobe snd_hda_intel enable=N
				MENU=0

		   elif [[ $userinput -eq 3 ]]
		   then

		  		printf "Bye Bye\n"
				MENU=0
				CONNECTED=0
			fi
		fi
    done
done


